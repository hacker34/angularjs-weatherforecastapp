/**
 * Created by jhacking on 5/10/2017.
 */

// Controller
weatherApp.controller('mainController', ['$scope', '$location', 'cityService', function ($scope, $location, cityService) {
    var vm = this;

    vm.city = cityService.city;

    $scope.$watch('mainControllerVm.city', function () {
        cityService.city = vm.city;
    });

    vm.submit = function () {
         $location.path("/forecast");
    }

}]);

weatherApp.controller('foreController', ['$scope', '$routeParams', 'cityService', 'weatherService', function ($scope, $routeParams, cityService, weatherService) {
    var vm2 = this;
    vm2.city = cityService.city;

    vm2.days = $routeParams.days || '2';

    vm2.weatherResult = weatherService.GetWeather(vm2.city,vm2.days);

    vm2.convertToFahrenheit = function(degK) {
        return Math.round((1.8 * (degK - 273)) + 32);
    }

    vm2.convertToDate = function (dt) {

        return new Date(dt * 1000);
    }

}]);