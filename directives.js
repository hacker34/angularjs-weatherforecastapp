/**
 * Created by jhacking on 5/10/2017.
 */

//Directives
weatherApp.directive("weatherReport", function () {
    return{
        restrict: 'E',
        templateUrl: 'directives/weatherReport.html',
        replace: true,
        scope: {
            weatherDay: "=",
            convertToStandard: "&",
            convertToDate: "&",
            dateFormat: "@"
        }
    }

})