/**
 * Created by jhacking on 5/10/2017.
 */
//Routes
weatherApp.config(function ($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'mainController',
            controllerAs: 'mainControllerVm'
        })

        .when('/forecast', {
            templateUrl: 'pages/forecast.html',
            controller: 'foreController',
            controllerAs: 'foreControllerVm'
        })

        .when('/forecast/:days', {
            templateUrl: 'pages/forecast.html',
            controller: 'foreController',
            controllerAs: 'foreControllerVm'
        })
});