/**
 * Created by jhacking on 5/10/2017.
 */
//Services
weatherApp.service('cityService', function () {
    this.city = "Vernal, UT";
})

weatherApp.service('weatherService', ['$resource', function($resource){
   this.GetWeather = function (city, days) {
       var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast/daily", {
           callback: "JSON_CALLBACK"
       }, {get: {method: "JSONP"}});

      return weatherAPI.get({q: city,cnt: days,appid: '3eeead2e516325791479bc322be57edf'});
   }

}]);