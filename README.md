# README #

This is a Weather Forecast App in Angular JS that I made for demo purposes.  It pulls from openweathermap API to retrieve the forecast data.  Anyone is welcome to download the code and implement themselves.

#### Code Info/Links #####

The code is broken down into different files by pages, controllers, directives, routes, and services.
Uses Angular 1.5.0-rc2 framework
http://openweathermap.org/